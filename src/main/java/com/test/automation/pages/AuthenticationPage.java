package com.test.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AuthenticationPage {
	
	WebDriver driver ;
	
	public AuthenticationPage(WebDriver driver)throws Exception {
		this.driver = driver;
	}
	
	public WebElement isPageAvailabile() throws Exception {
		WebElement ele = driver.findElement(By.id("center_column")); 
		return ele;
	}
	
	public void enterNewAccountEmail(String email) throws Exception{
		driver.findElement(By.id("email_create")).sendKeys(email);
	}
	
	public UserAccountCreationPage proccedToAccountCreationPage() throws Exception{
		
		UserAccountCreationPage createUserPage = new UserAccountCreationPage(driver);
		driver.findElement(By.id("SubmitCreate")).submit();
		return createUserPage;
		
	}
	
	public WebElement enterInvalidEmailForUserCreation(String email) throws Exception{
		//TODO: Implement the method
		return null;
	}

	public WebElement enterInvalidEmailForSignIn(String email) throws Exception{
		//TODO: Implement the method
		return null;
	}
	
	public WebElement enterInvalidPasswordForSignIn(String email) throws Exception{
		//TODO: Implement the method
		return null;
	}
	
	public void signInToAccount(String email , String password) throws Exception{
		//TODO: Implement the method
	}
	
}
