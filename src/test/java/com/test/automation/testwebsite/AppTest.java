package com.test.automation.testwebsite;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.test.automation.pages.AuthenticationPage;
import com.test.automation.pages.HomePage;
import com.test.automation.pages.UserAccountCreationPage;

public class AppTest {

	public WebDriver driver;
	public String driverPath = "\\Resourses\\chromedriver.exe";
	public String url = "http://automationpractice.com/index.php";


	
	private HomePage homePage ;
	private AuthenticationPage authUserPage  ;
	private UserAccountCreationPage createUserPage  ;

	@BeforeClass
	public void setup() {
		try {
			System.setProperty("webdriver.chrome.driver", driverPath);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(url);
		} catch (Exception e) {
			Assert.fail("Driver Setup failed ", e);
		}

	}

	@Test
	public void isSignupTheNewUser() {
		String email = "akila.samaranayake@colombo.rezgateway.com";
		String dob  = "28-10-1991";
		try {
			homePage = new HomePage(driver);
			assertTrue(homePage.isHomePageAvailabile().isDisplayed(), "Home Page is not Available");
			authUserPage = homePage.navigateToAccountPage();
			authUserPage.enterNewAccountEmail(email);
			createUserPage = authUserPage.proccedToAccountCreationPage();
			
			//set the values 
			assertTrue(createUserPage.isPageAvailbile().isDisplayed(), "User creation page is available");
			createUserPage.enterFirstName("Akila");
			createUserPage.enterLastName("Samaranayake");
			if(createUserPage.isEmailSelected().getText().equalsIgnoreCase(email)){
				System.out.println(">>>>>>>>>>>>> Successfully set user entered email >>>>>>>>>>");
			}else{
				System.out.println(">>>>>>>>>>>>> Not set the entered email >>>>>>>>>>");
			}
			createUserPage.enterTheNewPassword("123456");
			createUserPage.setDateOfBirth(dob);
			createUserPage.setCustomerCompanyName("Rez");
			createUserPage.setCustomerAddressLineOne("School lane");
			createUserPage.setCustomerAddressLineTwo("halpita");
			createUserPage.setCustomerCity("Colorado");
			createUserPage.setState("Colorado");
			createUserPage.enterMobilePhoneNumber("1111111111");
			assertTrue(createUserPage.clickTheRegisterButton().getAttribute("class").contains("danger"), "Enter details are wrong"+createUserPage.clickTheRegisterButton().getText());

			//for this point finished the 2 hours 
			
		} catch (Exception e) {
			Assert.fail("Testing failed Due to ", e);
		}

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
